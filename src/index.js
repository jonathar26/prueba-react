import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function FormattedDate(props){
    return <h2>Hora actual Colombia {props.date.toLocaleTimeString()}.</h2>
}

class Clock extends Component {
    constructor(props){
        super(props);
        this.state = {date : new Date()};
    }

    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick(){
        this.setState({date : new Date()});
    }

    render(){
        return (
            <div>
                <h1>Hola, Extraño!</h1>
                <FormattedDate date={this.state.date} />
            </div>
        );
    }
}

function App() {
    return(
    <div>
        <Clock />
        <Clock />
        <Clock />
    </div>
    );
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);

class Toggle extends Component {
    constructor(props){
        super(props);
        this.state = {isToggleOn : true};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    render(){
        return(
            <button onClick={this.handleClick}>
                {this.state.isToggleOn ? 'ON' : 'OFF'}
            </button>
        );
    }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')  
);

