import React from 'react';
import './index.css';

class Clock extends Component {
    render(){
        return (
            <div>
                <h1>Hello, World!</h1>
                <h2>Esto es {this.props.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}